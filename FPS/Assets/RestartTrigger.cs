﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartTrigger : MonoBehaviour {

    private Scene scene;

	// Use this for initialization
	void Start () {
        scene = SceneManager.GetActiveScene();
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Application.LoadLevel(scene.name);
        }
        else
        {

        }
    }
}

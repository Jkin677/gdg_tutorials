﻿using UnityEngine;
using System.Collections;

public class CreateEvilCube : MonoBehaviour
{
    public GameObject target;
    public float RandomSummonRange = 2f;

    void Start()
    {
        InvokeRepeating("SpawnObject", 2, 1);
    }

    void SpawnObject()
    {
        float x = Random.Range(-RandomSummonRange, RandomSummonRange);
        float z = Random.Range(-RandomSummonRange, RandomSummonRange);
        Instantiate(target, new Vector3(x, 10, z), Quaternion.identity);
    }
}